<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Requests\ClientCommentRequest;
use App\Http\Requests\OrderFilesRequest;
use App\Models\ClientBase;
use App\Services\ClientBaseControllerService;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Requests\ClientCreateFormRequest;
use App\Http\Requests\ClientUpdateFormRequest;


class ClientBaseController extends Controller
{
    private ClientBaseControllerService $clientBaseControllerService;

    public function __construct(ClientBaseControllerService $clientBaseControllerService)
    {
        $this->clientBaseControllerService = $clientBaseControllerService;
    }

    public function index(Request $request): View|Factory|\Illuminate\Foundation\Application|JsonResponse|Application
    {
        $clientQuery = $this->clientBaseControllerService->clientsDateFilters($request);

        return $this->clientBaseControllerService->clientIndexView($clientQuery);
    }

    public function search(SearchRequest $request): \Illuminate\Foundation\Application|Factory|View|JsonResponse|Application|RedirectResponse
    {
        try {
            $clientsQuery = $this->clientBaseControllerService->clientsSearch($request);
            return $this->clientBaseControllerService->clientIndexView($clientsQuery);
        } catch (Exception $e) {
            return back()->withErrors(['error' => "Произошла ошибка: " . $e->getMessage()]);
        }
    }

    public function edit(ClientBase $clientBase): View|\Illuminate\Foundation\Application|Factory|Application
    {
        return $this->clientBaseControllerService->clientEdit($clientBase);
    }

    public function update(ClientUpdateFormRequest $request, ClientBase $clientBase): RedirectResponse
    {
//        try {
            return $this->clientBaseControllerService->clientUpdate($request, $clientBase);
//        } catch (Exception $e) {
//            return back()->withErrors(['error' => "Произошла ошибка: " . $e->getMessage()]);
//        }
    }

    public function create(): View|\Illuminate\Foundation\Application|Factory|Application
    {
        return $this->clientBaseControllerService->clientCreate();
    }


    public function store(ClientCreateFormRequest $request): RedirectResponse
    {
        try {
            return $this->clientBaseControllerService->clientStore($request);
        } catch (Exception $e) {
            return back()->withErrors(['error' => "Произошла ошибка: " . $e->getMessage()]);
        }
    }


    public function delete(ClientBase $clientBase): RedirectResponse
    {
        return $this->clientBaseControllerService->clientDelete($clientBase);

    }

    public function clientContactDelete($id): RedirectResponse
    {
        return $this->clientBaseControllerService->contactDelete($id);
    }


    public function addComment(ClientCommentRequest $request, $id): RedirectResponse
    {
        return $this->clientBaseControllerService->clientAddComment($request, $id);

    }

    public function getClientContactInfo(Request $request): JsonResponse
    {
        return $this->clientBaseControllerService->clientContactInfo($request);
    }

    public function addFiles(OrderFilesRequest $request, $id): RedirectResponse
    {
        return $this->clientBaseControllerService->clientAddFiles($request, $id);
    }

    public function deleteFile(int $id): RedirectResponse
    {
        return $this->clientBaseControllerService->clientDeleteFile($id);
    }

    public function checkCompanyExists(Request $request): JsonResponse
    {
        return $this->clientBaseControllerService->checkCompany($request);

    }
    public function checkClientExists(Request $request): JsonResponse
    {
        return $this->clientBaseControllerService->checkClient($request);

    }
}
